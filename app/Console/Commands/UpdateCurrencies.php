<?php

namespace App\Console\Commands;

use App\Currency;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use SimpleXMLElement;

class UpdateCurrencies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:currencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Команда обновления данных в таблице currency';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line('------------- UpdateCurrencies started at '.date("d.m.Y h  H:i:s").'-------------');
        $res = Http::get('https://nationalbank.kz/rss/rates_all.xml?switch=russian');
        if ($res->getStatusCode() == 200) {
            $xml = new SimpleXMLElement($res->getBody()->getContents());
            $items = $xml->xpath('*/item');
            foreach($items as $item) {
                $this->line($item->title.': '.$item->description);
                $curr = new Currency();
                $curr->name = $item->title;
                $curr->rate = $item->description;
                $curr->date_ = Carbon::createFromFormat('d.m.Y', $item->pubDate)->format('Y-m-d');
                $curr->save();
            }
            $this->line('------------- UpdateCurrencies ended at '.date("d.m.Y H:i:s").' -------------');
        } else {
            $this->line('Get request status code: '.$res->getStatusCode());
        }
        return 0;
    }
}
